Por Paulo Henrique Alves de Deus.

Modelo adquirido em Free3D. Icone adquirido em FlatIcon.

Comandos:

Clique direito do mouse para mover lobo azul e capturar ossos.
Segurar roda do mouse para rotacionar câmera.
Girar roda do mouse para zoom.
Mover mouse até as bordas ou usar teclas W, A, S, e D para mover a câmera.

Objetivo:

Capturar os ossos e levá-los para a base (azul).
