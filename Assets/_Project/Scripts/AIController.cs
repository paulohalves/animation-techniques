﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    [SerializeField] private PlayerBase _base = default;
    [SerializeField] private WolfController _wolfController = default;
    [SerializeField] private float _maxDelay = 3.0f;
    [SerializeField, Range(0, 1.0f)] private float _aiEfficiency = 1.0f;

    private Coroutine _behaviourCoroutine;

    private readonly List<Bone> _bones = new List<Bone>();

    private void Start()
    {
        ResetBehaviour();
    }

    private IEnumerator WolfBehaviorCoroutine()
    {
        while (true)
        {
            bool walkingToBone;
            bool digging;
            bool walkingToBase;


            yield return new WaitForSeconds(_maxDelay - (_maxDelay * _aiEfficiency));

            Bone bone = GetBone();
            if (!bone) continue;

            yield return new WaitWhile(() => !_wolfController.CanMove);

            walkingToBone = true;
            var walkToBone = _wolfController.MoveTo(bone.transform.position);
            walkToBone.OnFinish(() => walkingToBone = false);
            walkToBone.OnCancel(ResetBehaviour);

            yield return null;

            yield return new WaitUntil(() => !walkingToBone && _wolfController.CanDig || !bone);

            if (!bone) continue;

            digging = true;
            var dig = _wolfController.Dig();
            dig.OnFinish(() => digging = false);
            dig.OnCancel(ResetBehaviour);

            yield return null;

            yield return new WaitUntil(() => !digging && _wolfController.CanMove);

            if (!_wolfController.CanReleaseBone) continue;

            walkingToBase = true;
            var walkToBase = _wolfController.MoveTo(_base.transform.position);
            walkToBase.OnFinish(() => walkingToBase = false);
            walkToBase.OnCancel(ResetBehaviour);

            yield return null;

            yield return new WaitUntil(() => !walkingToBase);

            _wolfController.ReleaseBone();

            yield return null;

            yield return new WaitUntil(() => !_wolfController.Howling);
        }
    }

    private void ResetBehaviour()
    {
        if (_behaviourCoroutine != null)
        {
            StopCoroutine(_behaviourCoroutine);
            _behaviourCoroutine = null;
        }

        _behaviourCoroutine = StartCoroutine(WolfBehaviorCoroutine());
    }

    private Bone GetBone()
    {
        _bones.Clear();
        _bones.AddRange(FindObjectsOfType<Bone>());
        _bones.Sort((a, b) => Vector3.Distance(_wolfController.transform.position, b.transform.position).CompareTo(Vector3.Distance(_wolfController.transform.position, a.transform.position)));

        if (_bones.Count == 0)
        {
            return null;
        }

        return _bones[Mathf.RoundToInt((_bones.Count - 1) * _aiEfficiency)];
    }
}
