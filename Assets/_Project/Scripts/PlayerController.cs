﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CameraController _cameraController = default;
    [SerializeField] private WolfController _wolfController = default;
    [SerializeField] private LayerMask _interactionLayer = default;

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Interact();
        }

        if (Input.GetMouseButton(2))
        {
            _cameraController.CameraRotate();
        }
        else
        {
            _cameraController.CameraZoom();
            _cameraController.CameraMove();
        }
    }

    private void Interact()
    {
        RaycastHit raycastHit;
        if (Physics.Raycast(_cameraController.GetCameraRay(), out raycastHit, 1000, _interactionLayer))
        {
            if (raycastHit.collider.gameObject.GetComponent<Bone>())
            {
                if (_wolfController.CanMove)
                {
                    WolfController.Action walk = _wolfController.MoveTo(raycastHit.point);
                    if (_wolfController.CanDig)
                    {
                        walk.OnFinish(() => _wolfController.Dig());
                    }
                }
            }
            else if (raycastHit.collider.gameObject.GetComponent<PlayerBase>())
            {
                if (_wolfController.CanMove)
                {
                    WolfController.Action walk = _wolfController.MoveTo(raycastHit.point);
                    if (_wolfController.CanReleaseBone)
                    {
                        walk.OnFinish(() => _wolfController.ReleaseBone());
                    }
                }
            }
            else
            {
                if (_wolfController.CanMove)
                {
                    WolfController.Action walk = _wolfController.MoveTo(raycastHit.point);
                }
            }
        }
    }
}
