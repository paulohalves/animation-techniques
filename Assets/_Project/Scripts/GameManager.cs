﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [System.Serializable]
    private class Wolf
    {
        public PlayerBase Base;
        public WolfController WolfController;
        public Text ScoreText;
        public int Score;
    }

    [SerializeField] private Arena _arena = default;
    [Header("Players")]
    [SerializeField] private Wolf[] _wolfs = default;
    [Header("Bones")]
    [SerializeField] private Object _bonePrefab = default;
    [SerializeField] private int _maxBoneCount = 10;
    [SerializeField] private float _boneSpawnInterval = 3.0f;

    private readonly List<Bone> _bones = new List<Bone>();

    private void Awake()
    {
        foreach(Wolf w in _wolfs)
        {
            w.WolfController.OnFinishDiggingBone += (wc, b) => HandleOnFinishDiggingBone(w, b);
            w.WolfController.OnReleaseBone += (wc) => HandleOnReleaseBone(w);
            w.ScoreText.text = $"Score: {w.Score}";
        }
    }

    private void Start()
    {
        for(int i = 0; i < _maxBoneCount / 2; i++)
        {
            SpawnBone();
        }
        StartCoroutine(BoneSpawnCoroutine());
    }

    private bool HandleOnReleaseBone(Wolf wolf)
    {
        if (wolf.Base.BoxCollider.bounds.Contains(wolf.WolfController.transform.position))
        {
            wolf.Score++;
            wolf.ScoreText.text = $"Score: {wolf.Score}";

            return true;
        }

        return false;
    }

    private bool HandleOnFinishDiggingBone(Wolf wolf, Bone bone)
    {
        if (bone)
        {
            _bones.Remove(bone);
            Destroy(bone.gameObject);
            return true;
        }
        return false;
    }

    private IEnumerator BoneSpawnCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(_boneSpawnInterval);
            if (_bones.Count >= _maxBoneCount)
            {
                yield return new WaitWhile(() => _bones.Count >= _maxBoneCount);
            }
            SpawnBone();
        }
    }

    private void SpawnBone()
    {
        GameObject boneObj = Instantiate(_bonePrefab) as GameObject;
        boneObj.transform.position = _arena.GetRandomSpawnPoint();
        _bones.Add(boneObj.GetComponent<Bone>());
    }
}
