﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour
{
    [SerializeField] private BoxCollider _spawnArea = default;

    public Vector3 GetRandomSpawnPoint()
    {
        return _spawnArea.bounds.center + new Vector3(
                   (Random.value - 0.5f) * _spawnArea.bounds.size.x,
                   0,
                   (Random.value - 0.5f) * _spawnArea.bounds.size.z
                );
    }
}
