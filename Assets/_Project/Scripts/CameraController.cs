﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _target = default;
    [SerializeField] private Transform _cameraArm = default;
    [SerializeField] private Camera _camera = default;
    [Space]
    [SerializeField] private float _scrollSpeed = 10.0f;
    [SerializeField] private float _movementSpeed = 10.0f;
    [SerializeField] private float _cameraMovementSpeed = 90.0f;
    [SerializeField] private float _mouseLimitsPercent = 0.1f;
    [SerializeField] private Vector3 _zoomLimits = new Vector2(-5, -20);

    private bool _followTarget = true;

    private void LateUpdate()
    {
        if (_followTarget)
        {
            transform.position = _target.position;
        }
    }

    public Ray GetCameraRay()
    {
        return _camera.ScreenPointToRay(Input.mousePosition);
    }

    public void CameraRotate()
    {
        float rotation = Input.GetAxis("Mouse X"); ;
        transform.Rotate(Vector3.up, rotation * _cameraMovementSpeed * Time.deltaTime);
    }

    public void CameraMove()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 movement = Vector3.zero;

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.z = Input.GetAxisRaw("Vertical");

        if (movement.sqrMagnitude == 0)
        {
            if (mousePos.x > 0 && mousePos.x < Screen.width)
            {
                if (mousePos.x < Screen.width * _mouseLimitsPercent)
                {
                    movement.x = -1;
                }
                else if (mousePos.x > Screen.width - (Screen.width * _mouseLimitsPercent))
                {
                    movement.x = 1;
                }
            }

            if (mousePos.y > 0 && mousePos.y < Screen.height)
            {
                if (mousePos.y < Screen.height * _mouseLimitsPercent)
                {
                    movement.z = -1;
                }
                else if (mousePos.y > Screen.height - (Screen.height * _mouseLimitsPercent))
                {
                    movement.z = 1;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _followTarget = true;
        }
        else if (movement.sqrMagnitude > 0)
        {
            _followTarget = false;
        }

        transform.position += transform.TransformDirection(movement) * _movementSpeed * Time.deltaTime;
    }

    public void CameraZoom()
    {
        Vector2 scroll = Input.mouseScrollDelta;
        float zoom = _camera.transform.localPosition.z + (scroll.y * _scrollSpeed * Time.deltaTime);
        zoom = Mathf.Clamp(zoom, _zoomLimits.y, _zoomLimits.x);

        _camera.transform.localPosition = Vector3.forward * zoom;
    }
}
