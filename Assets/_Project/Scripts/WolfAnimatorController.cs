﻿using UnityEngine;

public class WolfAnimatorController : MonoBehaviour
{
    [SerializeField] Animator _animator = default;

    private readonly int ForwardHash = Animator.StringToHash("Forward");
    private readonly int RightHash = Animator.StringToHash("Right");
    private readonly int DiggingHash = Animator.StringToHash("Digging");
    private readonly int SniffingHash = Animator.StringToHash("Sniffing");
    private readonly int OpenMouthHash = Animator.StringToHash("OpenMouth");
    private readonly int CloseMouthHash = Animator.StringToHash("CloseMouth");
    private readonly int HowlHash = Animator.StringToHash("Howl");

    public void Howl()
    {
        _animator.SetTrigger(HowlHash);
    }

    public void SetDigging(bool digging)
    {
        _animator.SetBool(SniffingHash, digging);
        _animator.SetBool(DiggingHash, digging);
    }

    public void SetForward(float forward)
    {
        _animator.SetFloat(ForwardHash, forward);
    }

    public void SetRight(float right)
    {
        _animator.SetFloat(RightHash, right);
    }

}
