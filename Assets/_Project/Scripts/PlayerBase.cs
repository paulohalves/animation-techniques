﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBase : MonoBehaviour
{
    [SerializeField] private Color _playerColor = default;
    [SerializeField] private Renderer[] _walls = default;
    [SerializeField] private BoxCollider _boxCollider = default;
    [SerializeField] private WolfController _wolfController = default;

    public BoxCollider BoxCollider => _boxCollider;

    private void Awake()
    {
        foreach(Renderer r in _walls)
        {
            r.material.color = _playerColor;
        }
    }

    private void Start()
    {
        Renderer[] renderers = _wolfController.RenderersToColor;

        foreach (Renderer r in renderers)
        {
            Color c = r.material.color;
            c = c * _playerColor;
            r.material.color = c;
        }
    }
}
