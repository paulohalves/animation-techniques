﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WolfController : MonoBehaviour
{
    public class Action
    {
        private System.Action _onFinish;
        private System.Action _onCancel;
        public Coroutine ActionCoroutine;

        public void Finish()
        {
            _onFinish?.Invoke();
        }

        public void OnFinish(System.Action action)
        {
            _onFinish += action;
        }

        public void Cancel()
        {
            _onCancel?.Invoke();
        }

        public void OnCancel(System.Action action)
        {
            _onCancel += action;
        }
    }

    public delegate bool WolfHandler(WolfController wolf);
    public delegate bool BoneHandler(WolfController wolf, Bone bone);
    public event BoneHandler OnFinishDiggingBone;
    public event WolfHandler OnReleaseBone;

    [SerializeField] private NavMeshAgent _agent = default;
    [SerializeField] private WolfAnimatorController _animatorController = default;
    [SerializeField] private WolfEventHandler _eventHandler = default;
    [SerializeField] private LayerMask _boneLayer = default;
    [SerializeField] private GameObject _bone = default;
    [SerializeField] private Renderer[] _renderersToColor = default;

    private bool _boneOnMouth = false;
    private bool _digging = false;
    private bool _howling = false;
    private float _currentAngularVelocity;
    private Vector3 _lastFacing;

    private Action _currentAction;

    public bool Moving => !_agent.isStopped;

    public bool CanMove => !_howling;
    public bool CanDig => !_boneOnMouth && !_howling;
    public bool CanReleaseBone => _boneOnMouth && !_howling;
    public bool BoneOnMouth { get => _boneOnMouth; private set => _boneOnMouth = value; }
    public bool Howling { get => _howling; private set => _howling = value; }
    public Renderer[] RenderersToColor => _renderersToColor;

    private void Awake()
    {
        _eventHandler.OnFinishDigging += HandleOnFinishDigging;
        _eventHandler.OnFinishHowling += HandleOnFinishHowling;
    }

    private void Start()
    {
        _agent.isStopped = true;
    }

    private void Update()
    {
        if (!_agent.isStopped)
        {
            if (ArrivedAtTarget())
            {
                _agent.isStopped = true;
            }

            Vector3 currentFacing = transform.forward;
            _currentAngularVelocity = Vector3.SignedAngle(currentFacing, _lastFacing, Vector3.up) / Time.deltaTime; //degrees per second
            _lastFacing = currentFacing;

            _animatorController.SetForward(1.0f);
            _animatorController.SetRight(-_currentAngularVelocity / _agent.angularSpeed);
        }
        else
        {
            _animatorController.SetForward(0.0f);
        }
    }

    private void HandleOnFinishDigging()
    {
        _digging = false;
        _animatorController.SetDigging(false);

        if (OnFinishDiggingBone != null)
        {
            if (OnFinishDiggingBone.Invoke(this, GetBone()))
            {
                SetBone(true);
            }
        }
    }

    private void HandleOnFinishHowling()
    {
        Howling = false;
    }

    private void Howl()
    {
        Howling = true;
        _animatorController.Howl();
    }

    private void SetBone(bool bone)
    {
        BoneOnMouth = bone;
        _bone.SetActive(bone);
    }

    public void ReleaseBone()
    {
        if (OnReleaseBone != null)
        {
            if (OnReleaseBone.Invoke(this))
            {
                SetBone(false);
                Howl();
            }
        }
    }

    public Action Dig()
    {
        if (!CanDig)
        {
            return null;
        }

        if (_currentAction != null)
        {
            StopCoroutine(_currentAction.ActionCoroutine);
            _currentAction.Cancel();
        }

        _currentAction = new Action();

        _currentAction.OnFinish(() => _currentAction = null);
        _currentAction.OnCancel(() => _digging = false);
        _currentAction.ActionCoroutine = StartCoroutine(DigCoroutine(_currentAction));

        return _currentAction;
    }

    public Action MoveTo(Vector3 position)
    {
        if (!CanMove)
        {
            return null;
        }

        if (_currentAction != null)
        {
            StopCoroutine(_currentAction.ActionCoroutine);
            _currentAction.Cancel();
        }

        _currentAction = new Action();

        _currentAction.OnFinish(() => _currentAction = null);
        _currentAction.ActionCoroutine = StartCoroutine(MoveToCoroutine(position, _currentAction));

        return _currentAction;
    }

    private IEnumerator DigCoroutine(Action action)
    {
        _digging = true;
        _animatorController.SetDigging(true);

        yield return new WaitUntil(() => !_digging);

        action.Finish();
    }

    private IEnumerator MoveToCoroutine(Vector3 position, Action action)
    {
        SetTarget(position);
        yield return new WaitUntil(ArrivedAtTarget);
        action.Finish();
    }

    private Bone GetBone()
    {
        Collider[] colliders = Physics.OverlapBox(transform.position, Vector3.one, Quaternion.identity, _boneLayer);

        foreach(Collider collider in colliders)
        {
            Bone bone = collider.GetComponent<Bone>();
            if (bone)
            {
                return bone;
            }
        }

        return null;
    }

    private bool ArrivedAtTarget()
    {
        return (Vector3.Distance(transform.position, _agent.destination) + _agent.baseOffset) <= _agent.stoppingDistance;
    }

    private void SetTarget(Vector3 position)
    {
        _agent.destination = position;
        _agent.isStopped = false;
    }
}
