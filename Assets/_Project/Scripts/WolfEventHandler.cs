﻿using UnityEngine;

public class WolfEventHandler : MonoBehaviour
{
    public delegate void FinishDiggingHandler();
    public event FinishDiggingHandler OnFinishDigging;
    public event FinishDiggingHandler OnFinishHowling;

    private void FinishDigging()
    {
        OnFinishDigging?.Invoke();
    }

    private void FinishHowl()
    {
        OnFinishHowling?.Invoke();
    }
}
